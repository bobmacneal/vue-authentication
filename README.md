# Vue Authentication

Basic template for Vue application with authentication against the Fastify endpoints found in 
[Authentication API Fastify](https://gitlab.com/bobmacneal/authentication-api-fastify)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
### SVG Generator
[svgrepo](https://www.svgrepo.com/collection/building-and-landscape-icons/)

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
