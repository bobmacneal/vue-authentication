import axios from "axios";

export const apiClient = axios.create();

const CancelToken = axios.CancelToken;
let cancel;

apiClient.interceptors.request.use(
  (config) => {
    if (cancel) {
      cancel(); // cancel request
    }
    config.cancelToken = new CancelToken(function executor(c) {
      cancel = c;
    });
    config.headers = {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    };
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);
