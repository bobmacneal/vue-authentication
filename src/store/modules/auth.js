const state = {
  user: null,
  accessToken: null,
  // token: window.localStorage.getItem('token')
};

const getters = {
  accessToken: (state) => state.accessToken,
  currentUser: (state) => state.user,
  isLoggedIn: (state) => !!state.accessToken,
};

const actions = {
  login: ({ commit }, { user, accessToken }) => {
    commit("setUser", user);
    commit("setToken", accessToken);
    // window.localStorage.removeItem('token');
  },
  logout: ({ commit }) => {
    commit("setUser", null);
    commit("setToken", null);
    // window.localStorage.removeItem('token');
  },
};

const mutations = {
  setToken: (state, accessToken) => {
    state.accessToken = accessToken;
  },
  setUser: (state, user) => {
    state.user = user;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
