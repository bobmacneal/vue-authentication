export const HttpStatus = {
  FORBIDDEN: 403,
  CONFLICT: 409,
  CREATED: 201,
  OK: 200,
};
